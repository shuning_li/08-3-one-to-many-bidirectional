package com.twuc.webApp.domain.oneToMany.bidirectionalMap;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ParentEntityRepositoryTest extends JpaTestBase {
    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Test
    void should_save_relationship_from_child_site() {
        // TODO
        //
        // 请书写测试实现如下的功能：
        //
        // Given parent 和 child 对象。并且它们均已经持久化。
        // When 我们在 child 对象上为其添加 parent 对象引用，并持久化后。
        // Then 当我们重新查询 parent 对象的时候，可以发现在 parent 对象的 children 列表中存在 child 对象。
        //
        // <--start-
        ClosureValue<Long> parentIdValue = new ClosureValue<>();
        ClosureValue<Long> childIdValue = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parent = parentEntityRepository.save(new ParentEntity("parent"));
            ChildEntity child = childEntityRepository.save(new ChildEntity("child"));

            parentIdValue.setValue(parent.getId());
            childIdValue.setValue(child.getId());
        });

        flushAndClear(em -> {
            ParentEntity parent = parentEntityRepository.getOne(parentIdValue.getValue());
            ChildEntity child = childEntityRepository.getOne(childIdValue.getValue());

            child.setParentEntity(parent);
        });

        run(em -> {
            ParentEntity parent = parentEntityRepository.getOne(parentIdValue.getValue());
            assertEquals(1, parent.getChildEntities().size());
        });
        // --end->
    }

    @Test
    void should_save_and_get_parent_and_child_relationship_at_once() {
        // TODO
        //
        // 请书写测试实现如下的功能：
        //
        // Given parent 和 child 对象。并且它们均没有被持久化。
        // When 我们在 parent 对象上为其添加 child 对象引用，并将 parent 对象持久化。
        // Then 当我们重新查询 child 对象的时候，可以发现 child 对象中存在对 parent 对象的引用。
        //
        // <--start-
        ParentEntity parent = new ParentEntity("parent");
        ChildEntity child = new ChildEntity("child");

        ClosureValue<Long> childIdValue = new ClosureValue<>();
        flushAndClear(em -> {
            parent.addChild(child);
            parentEntityRepository.save(parent);
            childIdValue.setValue(child.getId());
        });

        run(em -> {
            ChildEntity queryChild = childEntityRepository.getOne(childIdValue.getValue());

            assertNotNull(queryChild.getParentEntity());
        });
        // --end->

    }

    @Test
    void should_remove_child_item() {
        // TODO
        //
        // 请书写如下的测试。
        //
        // Given parent 和 child 对象，并且其已经相互引用并已经持久化。
        // When 当我们从 parent 的 children 列表中移除 child 对象并持久化时。
        // Then 当我们再次查询 parent 的时候，发现 parent 的 children 列表中为空。同时 child 记录本身也
        //   不复存在了。
        //
        // <--start-
        ClosureValue<Long> parentIdValue = new ClosureValue<>();
        ClosureValue<Long> childIdValue = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parent = new ParentEntity("parent");
            ChildEntity child = new ChildEntity("child");
            parent.addChild(child);

            parentEntityRepository.save(parent);
            parentIdValue.setValue(parent.getId());
            childIdValue.setValue(child.getId());
        });

        flushAndClear(em -> {
            ParentEntity parent = parentEntityRepository.getOne(parentIdValue.getValue());
            ChildEntity child = childEntityRepository.getOne(childIdValue.getValue());
            parent.removeChild(child);
        });

        run(em -> {
            ParentEntity parent = parentEntityRepository.getOne(parentIdValue.getValue());
            Optional<ChildEntity> optionalChild = childEntityRepository.findById(childIdValue.getValue());

            assertEquals(0, parent.getChildEntities().size());
            assertFalse(optionalChild.isPresent());
        });
        // --end->
    }
}
